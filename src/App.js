import './App.css';
import Movie from './components/Movie';
import PageWrapper from './components/PageWrapper';

function App() {
  const movies = [
    {
      imageMovie: "mv1",
      title: "oblivion",
      releaseYear: "2012",
      qualification: "8.1",
      runTime: "2h21’",
      classification: "PG-13",
      directors: ["Joss Whedon"],
      releaseDate: "1 May 2015",
      actors: ["Robert Downey Jr.", "Chris Evans", "Chris Hemsworth"],
      description: "Earth's mightiest heroes must come together and learn to fight as a team if they are to stop the mischievous Loki and his alien army from enslaving humanity..." 
    },
    {
      imageMovie: "mv2",
      title: "into the wild",
      releaseYear: "2014",
      qualification: "7.1",
      runTime: "2h21’",
      classification: "PG-13",
      directors: ["Anthony Russo", "Joe Russo"],
      releaseDate: "1 May 2015",
      actors: ["Robert Downey Jr.", "Chris Evans", "Chris Hemsworth"],
      description: "As Steve Rogers struggles to embrace his role in the modern world, he teams up with a fellow Avenger and S.H.I.E.L.D agent, Black Widow, to battle a new threat..." 
    },
    {
      imageMovie: "mv3",
      title: "blade runner",
      releaseYear: "2015",
      qualification: "7.3",
      runTime: "2h21’",
      classification: "PG-13",
      directors: ["Peyton Reed"],
      releaseDate: "1 May 2015",
      actors: ["Paul Rudd", "Michael Douglas"],
      description: "Armed with a super-suit with the astonishing ability to shrink in scale but increase in strength, cat burglar Scott Lang must embrace his inner hero and help..."
    },
    {
      imageMovie: "mv4",
      title: "Mulholland pride",
      releaseYear: "2013",
      qualification: "7.2",
      runTime: "2h21’",
      classification: "PG-13",
      directors: ["Shane Black"],
      releaseDate: "1 May 2015",
      actors: ["Robert Downey Jr.", "Guy Pearce", "Don Cheadle"],
      description: "When Tony Stark's world is torn apart by a formidable terrorist called the Mandarin, he starts an odyssey of rebuilding and retribution."
    },
    {
      imageMovie: "mv5",
      title: "skyfall: evil of boss",
      releaseYear: "2013",
      qualification: "7.0",
      runTime: "2h21’",
      classification: "PG-13",
      directors: ["Shane Black"],
      releaseDate: "1 May 2015",
      actors: ["Chris Hemsworth", "Natalie Portman", "Tom Hiddleston"],
      description: "When Tony Stark's world is torn apart by a formidable terrorist called the Mandarin, he starts an odyssey of rebuilding and retribution."
    }
  ];



  return (
    <PageWrapper>

      {movies.map( (movie, index) => {
        return <Movie
                key={index}
                imageMovie={movie.imageMovie}
                title={movie.title}
                releaseYear={movie.releaseYear}
                qualification={movie.qualification}
                runTime={movie.runTime}
                classification={movie.classification}
                directors={movie.directors}
                releaseDate={movie.releaseDate}
                actors={movie.actors}>
                {movie.description}
              </Movie>
      })}    
      
    </PageWrapper>
  );
}

export default App;
