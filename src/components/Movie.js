import React from "react";

function Movie(props) {
  
  const actors = props.actors;
  const actorsItems = actors.map((actor, index) => {
    return index +1 !== actors.length ?
      <a href="" key={index + actor.toString()}>{actor}, </a> 
    :
      <a href="" key={index + actor.toString()}>{actor}</a>
    
  });

  const directors = props.directors;
  const directorsItems = directors.map((director, index) => {
    return index +1 !== directors.length ?
      <a href="" key={index + director.toString()}>{director}, </a> 
    :
      <a href="" key={index + director.toString()}>{director}</a>
    
  });

  return (
    <div className="movie-item-style-2">
      <img src={require(`../../public/images/uploads/${props.imageMovie}.jpg`)} alt="" />
      <div className="mv-item-infor">
        <h6>
          <a href="moviesingle.html">
            {props.title} <span>({props.releaseYear})</span>
          </a>
        </h6>
        <p className="rate">
          <i className="ion-android-star"></i>
          <span>{props.qualification}</span> /10
        </p>
        <p className="describe">{props.children}</p>
        <p className="run-time"> Run Time: {props.runTime}    .     <span>MMPA: {props.classification} </span>    .     <span>Release: {props.releaseDate}</span></p>
        <p>Director: {directorsItems}</p>
        <p>Stars: {actorsItems}</p>
      </div>
    </div>
  );
}

export default Movie;